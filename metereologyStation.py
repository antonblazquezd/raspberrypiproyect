import adafruit_dht
import board
import time

sensor = adafruit_dht.DHT11(board.D24, use_pulseio = False)

for i in range(10):
    try:
        temp = sensor.temperature
        hum = sensor.humidity
        print('Temp:', temp, 'Hum :', hum)
        time.sleep(0.5)
    except:
        print("Lectura incorrecta!")

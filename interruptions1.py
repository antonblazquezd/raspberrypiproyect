import RPi. GPIO as GPIO
import time

if __name__ == '__main__':
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(16, GPIO.IN , pull_up_down = GPIO.PUD_UP)

    pressed = False
    while True:
        if not  GPIO.input(16):
            if not pressed:
                print("You've pressed the button!")
                pressed = True
        else:
            pressed = False
        time.sleep(0.1)















"""
#4
def click(channel):
    if GPIO.input(IN) == GPIO.LOW:
        print("ON")
        GPIO.output(OUT.GPIO.HIGH)
    else:
        print("OFF")
        GPIO.output(OUT, GPIO.LOW)
"""

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
IN = 16
OUT = 18
GPIO.setup(IN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(OUT, GPIO.OUT)

#1
#Si mantengo pulsado se enciende la luz cuando dejo de pulsar se apaga
i = 1
try:
    while True:
        if GPIO.input(IN) == GPIO.LOW:
            print("ON", i)
            GPIO.output(OUT, GPIO.HIGH)
        else:
            print("OFF", i)
            GPIO.output(OUT, GPIO.LOW)
        time.sleep(0.1)
        i += 1
finally:
    GPIO.cleanup()
    print("Saliendo...")

"""
#2
#Si pulso se enciende y si vuelvo a pulsar se apaga
pulsado = GPIO.input(IN)
encendido = False
try:
    while  True:
        if GPIO.input(IN) != pulsado and GPIO.input(IN) == GPIO.LOW:
            if encendido:
                GPIO.output(OUT, GPIO.LOW)
                encendido = False
            else:
                GPIO.output(OUT, GPIO.HIGH)
                encendido = True
            time.sleep(0.1)




#3
#Esta en espera hasta que pulso el boton
i=1
try:
    while True:
        GPIO.wait_for_edge(IN, GPIO.BOTH) #GPIO.RISING  GPIO.FALLING
        if GPIO.input(IN) == GPIO.LOW:
            print("ON", i)
            GPIO.output(OUT, GPIO.HIGH)
        else:
            print("OFF", i)
            GPIO.output(OUT, GPIO.LOW)
        i+=1


"""

"""
#4
try:
    GPIO.add_event_detect(IN, GPIO.BOTH, callback = click, bouncetime = 100)
    while True:
        pass
finally:
    GPIO.cleanup()
    print("Saliendo...")
"""
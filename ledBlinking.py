import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(18,GPIO.OUT)
print('Led On')
GPIO.output(18,GPIO.HIGH)
time.sleep(1)
print('Led Off')
GPIO.output(18,GPIO.LOW)